#!/bin/bash

# Copyright (C) 2012-2013  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# USAGE: ./run-built-arm-image.sh KERNEL ROOTFS [VERSION]

KERNEL="$1"
ROOTFS="$2"
VERSION="factory"
if [ -n "$3" ]; then
    VERSION="$3"
fi

find_tunctl() {
    for path in `which tunctl` /{usr/,usr/local/}{bin,sbin}/tunctl; do
        if [ -x "$path" ]; then
            return 0
	fi
    done
    return 1
}

if ! find_tunctl; then
cat >&2 <<EOF
Could not find tunctl executable in standard paths or PATH

On Fedora it is in the tunctl package.
On Ubuntu it is in the uml-utilities package.
EOF
fi

MACHINE=qemuarmv7 PATH="$PATH:$(pwd)" OE_TMPDIR=/ KERNEL="$1" ROOTFS="$2" \
    sh ./runqemu serial \
        bootparams="rootflags=subvol=systems/$VERSION/run root=/dev/mmcblk0" \
	btrfs qemuparams="-m 1024 -M vexpress-a9"
